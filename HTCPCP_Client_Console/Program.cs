﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace HTCPCP_Client_Console
{
    class Program
    {
        static void test()
        {
            using (HTCPCPClient m = new HTCPCPClient("vps.hbristow.eu"))
            {
                m.addAddition("Cream", "splash");
                m.addAddition("Cinnamon", 2);
                m.addAddition("Vanilla", "lots");

                m.setLanguage("Bosnian");

                Console.WriteLine("{0}", m.brew() ? "BREW method sent." : "Socket error.");

                //Console.WriteLine("{0}", m.get() ? "GET method sent." : "Socket error.");

                //Console.WriteLine("{0}", m.post() ? "POST method sent." : "Socket error.");

                //Console.WriteLine("PROPFIND RESPONSE: {0}", m.propfind());
            }

            Console.Read();
        }

        static void Main(string[] args)
        {
            test();
            Console.Read();
        }
    }
}
