﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace HTCPCP_Client_Console
{
    /// <summary>
    /// Facilitates the formulation of HTCPCP Requests
    /// </summary>
    class HTCPCPClient : IDisposable
    {
        public string target { get; set; }
        public Int32 port { get; set; }

        private Dictionary<string, string> coffee_schemes = new Dictionary<string, string>()
        #region coffee_schemes
        {
		            {"Afrikaans", "koffie"},
            {"Dutch", "koffie"},
            {"Azerbaijani", "q%C3%A6hv%C3%A6"},
            {"Arabic", "%D9%82%D9%87%D9%88%D8%A9"},
            {"Basque", "akeita"},
            {"Bengali", "koffee"},
            {"Bosnian", "kahva"},
            {"Bulgarian", "kafe"},
            {"Catalan", "caf%C3%E8"},
            {"French", "caf%C3%E8"},
            {"Galician", "caf%C3%E8"},
            {"Chinese", "%E5%92%96%E5%95%A1"},
            {"Croatian", "kava"},
            {"Czech", "k%C3%A1va"},
            {"Danish", "kaffe"},
            {"Norwegian", "kaffe"},
            {"Swedish", "kaffe"},
            {"English", "coffee"},
            {"Esperanto", "kafo"},
            {"Estonian", "kohv"},
            {"Finnish", "kahvi"},
            {"German", "%4Baffee"},
            {"Greek", "%CE%BA%CE%B1%CF%86%CE%AD"},
            {"Hindi", "%E0%A4%95%E0%A5%8C%E0%A4%AB%E0%A5%80"},
            {"Japanese", "%E3%82%B3%E3%83%BC%E3%83%92%E3%83%BC"},
            {"Korean", "%EC%BB%A4%ED%94%BC"},
            {"Russian", "%D0%BA%D0%BE%D1%84%D0%B5"},
            {"Thai", "%E0%B8%81%E0%B8%B2%E0%B9%81%E0%B8%9F"} 
        };
        #endregion

        private const string addition_types = "milk-type syrup-type sweetener-type spice-type alcohol-type";
        private const string milk_type = "Cream Half-and-half Whole-milk Part-skim Skim Non-dairy";
        private const string syrup_type = "Vanilla Almond Raspberry";
        private const string sweetener_type = "White-sugar Sweetener Raw-cane Honey";
        private const string spice_type = "Cinnamon Cardamon";
        private const string alcohol_type = "Brandy Rum Whiskey Aquavit Kahlua";
        private const string volume = "dash splash little medium lots";
        private string[] addition_type = new string[] { 
            milk_type, syrup_type, sweetener_type, spice_type, alcohol_type};

        private string s_Accept_Additions = "";
        private string s_URI = "";
        private string s_potDesignator = "pot-1";
        private string s_language = "English";

        public HTCPCPClient(string target)
        {
            this.target = target;
            this.port = 80;
        }

        // Configuration methods

        public bool addAddition(string addition, string parameter)
        {
            if (additionExists(addition) && additionParameterExists(parameter))
            {
                s_Accept_Additions += addition + ";" + parameter + ",";

                // Rewrite so that , is not suffixed, but prefixed when needed

                return true;
            }

            return false;
        }

        public bool addAddition(string addition, int parameter) // To merge with addAddition(string, string)
        {
            
            return addAddition(addition, parameter.ToString());
        }

        public bool setLanguage(string language)
        {
            foreach (KeyValuePair<string, string> pair in coffee_schemes) 
                if (pair.Key.Equals(language))
                {
                    this.s_language = language;
                    return true;
                }

            return false;
        }

        // HTTP Methods

        public bool brew(bool isPost = false)
        {
            string request =
                request_line((isPost ? "POST " : "BREW ")) +
                (s_Accept_Additions != "" ? s_Accept_Additions.Remove(s_Accept_Additions.Length - 1) + System.Environment.NewLine : "") +
                "Content-Type: message/coffeepot " + System.Environment.NewLine +
                "coffee-message-body: start" + System.Environment.NewLine;

            return tcp_tx(this.target, this.port, request);
        }

        public bool post()
        {
            // BREW and POST are functionally identidical, but differ in the request string format
            return brew(true);
        }

        public bool get()
        {
            string request = request_line("GET");

            return tcp_tx(this.target, this.port, request);
        }

        public bool when()
        {
            string request = request_line("WHEN");

            return tcp_tx(this.target, this.port, request);
        }

        public string propfind()
        {
            string request = request_line("PROPFIND");

            return tcp_txrx(this.target, this.port, request);
        }

        // Method Utilities

        private bool additionExists(string addition)
        {
            foreach (string each_addition_type in addition_type)
                if (each_addition_type.Contains(addition))
                    return true;
            return false;
        }

        private bool additionParameterExists(string parameter)
        {
            int t;
            if (volume.Contains(parameter) || Int32.TryParse(parameter, out t))
                return true;
            return false;
        }

        private string request_line(string method)
        {
            return method + " " + buildURI() + " 0.1" + System.Environment.NewLine;
        }

        private string getScheme(string language)
        {
            string rtn = "coffee";

            coffee_schemes.TryGetValue(language, out rtn);

            return rtn + @"://";
        }

        private string buildURI()
        {
            this.s_URI = getScheme(this.s_language) + target + (port.Equals(80) ? "" : ":" + port) +
                "/" + s_potDesignator +
                (s_Accept_Additions.Equals("") ? "" : "?" + s_Accept_Additions.Remove(s_Accept_Additions.Length - 1));

            return this.s_URI;
        }

        // Network Methods

        /// <summary>
        /// Bidirectional TCP communication (Tx then listen for reply)
        /// </summary>
        /// <param name="target"></param>
        /// <param name="port"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private string tcp_txrx(String target, Int32 port, string msg)
        {
            TcpClient tcp_c;

            try
            {
                tcp_c = new TcpClient(target, port);
            }
            catch (SocketException)
            {
                return null;
            }

            // Translate the passed message into ASCII and store it as a Byte array.
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Create a r/w stream
            NetworkStream stream = tcp_c.GetStream();

            stream.Write(data, 0, data.Length);

            Console.WriteLine("Tx:\n{0}\n--------", msg);


            data = new Byte[256];

            String responseData = String.Empty;

            // Will hang if no response - introduce timeout?
            Int32 bytes = stream.Read(data, 0, data.Length);
            
            responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

            Console.WriteLine("Rx:\n{0}\n--------", responseData);

            //Cleanup
            stream.Close();
            tcp_c.Close();

            return responseData;
        }

        /// <summary>
        /// Unidirectional TCP message, no wait for reply
        /// </summary>
        /// <param name="target"></param>
        /// <param name="port"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private bool tcp_tx(String target, Int32 port, string msg)
        {
            TcpClient tcp_c;

            try
            {
                tcp_c = new TcpClient(target, port);
            }
            catch (SocketException)
            {
                return false;
            }

            // Translate the passed message into ASCII and store it as a Byte array.
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Create a r/w stream
            NetworkStream stream = tcp_c.GetStream();

            stream.Write(data, 0, data.Length);

            Console.WriteLine("Tx:\n{0}\n--------", msg);

            //Cleanup
            stream.Close();
            tcp_c.Close();

            return true;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

}
