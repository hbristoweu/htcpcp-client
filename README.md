# README #

A C# class implementation of an RFC 2324 compliant client for HTCPCP requests.

![htcpcp-client2.png](https://bitbucket.org/repo/XBnzeL/images/4088166012-htcpcp-client2.png)

### Building ###

This project was built in VS2010 C# Express.